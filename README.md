# AI for Games

The implementation of assignments from the [course](https://is.muni.cz/predmet/fi/jaro2021/PA217).

- Task 1
  - [x]  Steering
  - [x] Obstacle Avoidance
  - [x] Flocking
- Task 2
  - [x] Pathfinding
  - [x] Behaviour Trees
