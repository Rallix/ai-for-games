﻿using System.Collections.Generic;
using UnityEngine;

namespace AI.Navigation
{
    abstract class Pathfinding
    { 
        protected Maze Maze { get; }

        protected Pathfinding(Maze maze)
        {
            Maze = maze;
        }

        public abstract Queue<Vector2Int> FindPath(Vector2Int start, Vector2Int end);

        protected abstract Queue<Vector2Int> ReconstructPath(Dictionary<Vector2Int, Vector2Int> cameFrom, 
                                                          Vector2Int current, Vector2Int start);
    }
}
