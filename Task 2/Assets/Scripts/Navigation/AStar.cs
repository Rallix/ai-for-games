﻿using System;
using System.Collections.Generic;
using System.Linq;
using Priority_Queue;
using UnityEngine;

namespace AI.Navigation
{
    //! Task 1.1: Why A* for pathfinding?

    // It's complete and will always be able to find a solution.
    // Also fiddling with parameters and heuristics can be used to simulate other algorithms.
    // "No other algorithm guarantees to expand less nodes than the A* algorithm."

    // A* is trying to reach the tile we want, instead of all nearby tiles until we finally find the target.
    // This means A* is a better all-purpose algorithm. It might be outperformed by BFS if we need to find paths
    // to a large number of targets, but there aren't that many collectibles for it to matter (if this changes,
    // and there are dozens and dozens of collectibles being spawned, we should consider changing our approach, too).

    // Also if we ever need to add a different tile type (e.g. water, with navigation cost of 2) in the future,
    // BFS wouldn't work as-is, and we would need to preprocess the maze "graph" first for it to work.
    // The maze is also tile-based, which means the big memory cost of BFS would quickly grow to be a problem
    // on larger graphs.

    // Since both A* and Dijkstra use priority queue, and A* is more or less Dijkstra with a heuristic method,
    // it will likely outperform it in most cases.

    // We don't need path smoothing, because the agent is intended to move in four directions only, step by step.

    //! Task 1.0: A*

    /// <summary> Finds the shortest path in a weighted graph. </summary>
    class AStar : Pathfinding
    {
        readonly Func<Vector2Int, Vector2Int, float> heuristic;

        public AStar(Maze maze, Func<Vector2Int, Vector2Int, float> heuristic) : base(maze)
        {
            this.heuristic = heuristic;
        }

        /// <summary> Starts the A* algorithm with plain aerial distance as heuristics. </summary>
        /// <param name="maze"></param>
        public AStar(Maze maze) : this(maze, (start, end) => Vector2.Distance(start, end)) { }


        public override Queue<Vector2Int> FindPath(Vector2Int start, Vector2Int end)
        {
            //! Initialization
            var closed = new List<Vector2Int>();
            var open = new SimplePriorityQueue<Vector2Int>();
            open.Enqueue(start, heuristic(start, end));

            var cameFrom = new Dictionary<Vector2Int, Vector2Int>();

            var gScore = new Dictionary<Vector2Int, float>();
            for (int row = 0; row < Maze.MazeTiles.Count; row++)
            {
                for (int col = 0; col < Maze.MazeTiles[row].Count; col++)
                {
                    gScore.Add(new Vector2Int(row, col), Mathf.Infinity);
                }
            }
            gScore[start] = 0;

            //! Find path
            while (open.Count > 0)
            {
                Vector2Int current = open.Dequeue();
                if (current == end) return ReconstructPath(cameFrom, current, start); // DONE

                closed.Add(current);
                foreach (Vector2Int neighbour in Maze.GetNeighbours(current))
                {
                    if (closed.Contains(neighbour)) continue; // already processed
                    if (!open.Contains(neighbour))
                    {
                        open.Enqueue(neighbour, gScore[neighbour] + heuristic(neighbour, end));
                    }
                    float score = gScore[current] + heuristic(current, neighbour);
                    if (score >= gScore[neighbour]) continue; // not better

                    // Accept
                    cameFrom[neighbour] = current;
                    gScore[neighbour] = score;
                    open.UpdatePriority(neighbour, gScore[neighbour] + heuristic(neighbour, end));
                }
            }
            return new Queue<Vector2Int>();
        }

        protected override Queue<Vector2Int> ReconstructPath(Dictionary<Vector2Int, Vector2Int> cameFrom, Vector2Int current, Vector2Int start)
        {
            var path = new Queue<Vector2Int>();
            path.Enqueue(current);

            while (current != start)
            {
                foreach (Vector2Int tile in cameFrom.Keys)
                {
                    if (tile == current)
                    {
                        current = cameFrom[tile];
                        path.Enqueue(current);
                    }
                }
            }
            var finalPath = new Queue<Vector2Int>(path.Reverse());
            if (finalPath.Count > 0) finalPath.Dequeue(); // We don't need to move to the tile we're already standing on
            return finalPath;
        }
    }
}
