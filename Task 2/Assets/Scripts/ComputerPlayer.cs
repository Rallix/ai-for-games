﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AI.Navigation;
using AI.Behaviour;
using AI.Behaviour.Actions;
using AI.Behaviour.Actions.Debugging;
using AI.Behaviour.Conditions;
using AI.Behaviour.Processors;

using UnityEngine;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

public class ComputerPlayer : AbstractPlayer
{
    Queue<Vector2Int> pathTilesQueue = new Queue<Vector2Int>();
    Task behaviourTree;

    public Vector2Int Position => parentMaze.GetMazeTileForWorldPosition(transform.position);
    public bool IsMoving => pathTilesQueue.Any();

    [SerializeField, Tooltip("Log the progress of the behaviour tree.")] bool log = true;
    [SerializeField, Range(0, 20), Tooltip("Maximal detour the computer is willing to take to collect B.")] int maxDetour = 8;

    public override void OnGameStarted()
    {
        base.OnGameStarted();
        Task.EnableLogs = log;
        behaviourTree = ConstructBehaviourTree();
        behaviourTree.Start();
    }


    protected override void Update()
    {
        base.Update();

        EvaluateDecisions(
                          parentMaze,
                          GameManager.Instance.HumanPlayer,
                          GameManager.Instance.SpawnedCollectibles,
                          GameManager.Instance.TimeRemaining
                         );
    }

    protected override Vector2Int GetNextPathTile()
    {
        if (pathTilesQueue.Count > 0)
        {
            return pathTilesQueue.Dequeue();
        }

        return base.GetNextPathTile();
    }

    protected override void StartMovementTransitionToNeighboringTile(Vector2Int tile)
    {
        // When rapidly changing directions at corners, sometimes the queue might
        // have distant tiles next to each other
        // It's fine; because we change the queue all the time
        if (Vector2Int.Distance(tile, CurrentTile) > 1 ||
            !parentMaze.IsValidTileOfType(tile, MazeTileType.Free))
        {
            pathTilesQueue.Clear();
            return;
        }
        transitionEndTile = tile;
        nextWorldMovePos = parentMaze.GetWorldPositionForMazeTile(tile);
    }

    /// <summary> Makes a step to the specified tile, which should be in the close vicinity. </summary>
    /// <param name="tile"> A tile which should be only one step to the left, right, up or down.</param>
    public void MakeStep(Vector2Int tile)
    {
        if (Vector2Int.Distance(tile, CurrentTile) > 1) return; // can sometimes happen when changing directions rapidly
        pathTilesQueue.Enqueue(tile);
    }

    private void EvaluateDecisions(Maze maze, AbstractPlayer humanPlayer,
                                   List<CollectibleItem> spawnedCollectibles, float remainingGameTime)
    {
        // TODO: To complete this assignment, you will need to write 
        //       a fair amount of code. It is recommended to create
        //       custom classes/functions to decouple the computations.

        //       This method should be the place where the final decision
        //       should be computed. The bot automatically follows the path which is
        //       described in the "pathTilesQueue" variable. All neighboring values inside
        //       this queue must be 4-neighbors, i.e., the bot can walk only
        //       up/down/left/right with a step of one.
        //      
        //       You do not have to use all arguments of this function 
        //       and you can add even more parameters if you would like to do so.
        //       Tiles of the maze can be accessed via maze.MazeTiles.
        //       Human player's tile location is available via humanPlayer.CurrentTile.
        //       CollectibleItem class contains TileLocation property providing information about collectible's position
        //       and Type property retrieving its type.
        //       Good luck. May your bot be the one who remains unbeaten by a human player. :)

        Task.EnableLogs = log;
        var info = new GameState(maze, humanPlayer, this, spawnedCollectibles, remainingGameTime);
        if (behaviourTree.IsRunning) behaviourTree.Update(info);
    }

    /// <summary> Creates a specific behaviour tree for this scenario. </summary>
    /// <example> See 'Tree.jpg' for visualisation. </example>
    /// <remarks> This should really be a created <see cref="ScriptableObject"/> instead. </remarks>
    Task ConstructBehaviourTree()
    {
        //! "Slow down Player by collecting B"
        // var scream1 = new Scream(Scream.ScreamVariant.Aah);
        var playerIsStoppable = new PlayerCanBeStopped(parentMaze);
        // A is priority -> don't go too far for B | search for Bs includes As, too
        var navigateToB = new NavigateToClosest(parentMaze, CollectibleItemType.RespawnAll, maxDetour);
        var sequenceB = new Sequence(playerIsStoppable, navigateToB);

        //! "Closest A isn't optimal A" (this is solved inside "NavigateTo" directly)
        // var scream2 = new Scream(Scream.ScreamVariant.Ook);
        // var sequence2 = new Sequence(new Fail(), scream2, scream2);

        // Possibly: prioritize larger clusters over the closest As
        // quite costly calculation

        //! "Closest A" (no condition --> prioritized if nothing else is happening)
        // var scream3 = new Scream(Scream.ScreamVariant.Eek);
        var grabA = new NavigateToClosest(parentMaze, CollectibleItemType.AddPoint);

        //! "When there are no valid As, grab the nearest respawn to create new As"
        var grabB = new NavigateToClosest(parentMaze, CollectibleItemType.RespawnAll);

        //! Succeed (we don't want to freeze forever if there are currently no valid collectibles)
        var success = new Succeed();

        // Also optimizing this to the point in which the computer is completely relentless
        // would be a disservice to the player (and the game's enjoyability) 
        // if he couldn't win even after spending reasonable effort.

        var mainSelector = new Selector(sequenceB, grabA, grabB, success);
        var root = new Repeat(mainSelector);
        return root;
    }

    [ContextMenu("AI / Switch on\\off", false, 10)]
    void SwitchAI()
    {
        if (behaviourTree.IsRunning)
        {
            behaviourTree.Reset(); // = "stop"
        }
        else
        {
            behaviourTree.Reset();
            behaviourTree.Start(); // = "restart"
        }
    }

    [ContextMenu("AI / Switch on\\off", true)]
    bool SwitchAIValidation()
    {
        return Application.isPlaying && behaviourTree != null;
    }


    #region Pathfinding Testing

    [ContextMenu("AI / Make Random Journey")]
    void GoJourney()
    {
        int maxIterations = 1000;
        Vector2Int final;
        Vector2Int current = Position;
        do
        {
            int row = Random.Range(0, parentMaze.MazeTiles.Count);
            int col = Random.Range(0, parentMaze.MazeTiles[row].Count);
            final = new Vector2Int(row, col);
            if (final == current) continue; // don't pick the tile we're already standing on

            if (--maxIterations < 0) throw new InvalidOperationException("Unable to find a valid random tile in the maze.");
        } while (!parentMaze.IsValidTileOfType(final, MazeTileType.Free));

        Debug.Log($"Going to tile: [{final.y}][{final.x}].", gameObject);

        Pathfinding pathfinding = new AStar(parentMaze);
        Queue<Vector2Int> path = pathfinding.FindPath(current, final);
        pathTilesQueue = new Queue<Vector2Int>(path);
    }

    [ContextMenu("AI / Make Random Step")]
    void GoStep()
    {
        int maxIterations = 100;
        Vector2Int next;
        do
        {
            Vector2[] directions = {Vector2.left, Vector2.up, Vector2.right, Vector2.down};
            Vector2 direction = directions[Random.Range(0, directions.Length)];

            Vector3 nextPosition = transform.position + (Vector3) direction;
            next = parentMaze.GetMazeTileForWorldPosition(nextPosition);

            if (--maxIterations < 0) throw new InvalidOperationException("Unable to find a valid random tile close to the player.");
        } while (!parentMaze.IsValidTileOfType(next, MazeTileType.Free));

        Debug.Log($"Going to tile: [{next.y}][{next.x}].", gameObject);
        MakeStep(next);
    }

    #endregion Pathfinding testing
}
