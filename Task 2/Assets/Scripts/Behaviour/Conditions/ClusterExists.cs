﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AI.Behaviour.Conditions
{
    /// <summary> Checks if a large group of As exists close together. </summary>
    class ClusterExists : Condition
    {

        int minClusterSize;
        int maxDistance;

        public ClusterExists(int clusterSize = 4, int maxDistance = 8)
        {
            this.minClusterSize = clusterSize;
            this.maxDistance = maxDistance;
        }

        /// <summary> The action performed by a task while it is running. </summary>
        public override void Update(GameState game)
        {
            IEnumerable<CollectibleItem> addPoints = game.spawnedCollectibles
                                                         .Where(p => p.Type == CollectibleItemType.AddPoint);

            // Calculating every shortest path between every point could cause a drop in performance
            // so we might be better of with simple Vector.Distance
            // Although it's a maze, so the above heuristic might not be very useful, and there aren't
            // that many collectibles at the same time anyway

            // TODO: Determine if there is a cluster of size 'clusterSize'+,
            // with collectibles within 'maxDistance' from each other

            throw new NotImplementedException();
        }
    }
}
