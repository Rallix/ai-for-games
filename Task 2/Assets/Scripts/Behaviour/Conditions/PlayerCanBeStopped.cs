﻿using AI.Navigation;
using UnityEngine;

namespace AI.Behaviour.Conditions
{
    /// <summary> The enemy is closer to B than the player is closer to A. </summary>
    class PlayerCanBeStopped : Condition
    {

        readonly Pathfinding pathfinding;
        const float TimeToMoveOneTile = 0.20f;

        public PlayerCanBeStopped(Maze maze) // max distance to B
        {
            pathfinding = new AStar(maze);
        }


        /// <summary> The action performed by a task while it is running. </summary>
        public override void Update(GameState game)
        {
            //! This alone ensures we don't pursue collectibles we can't get to in time. (Task 2.3)
            bool WillExpireSoon(CollectibleItem collectible, int distance)
            {
                return collectible.MarkedForDestroy || collectible.RemainingLifetime < (distance + 1) * TimeToMoveOneTile;
            }

            int minComputerToB = int.MaxValue;
            foreach (CollectibleItem collectible in game.spawnedCollectibles)
            {
                if (collectible.Type != CollectibleItemType.RespawnAll) continue;

                int computerToBDistance = pathfinding.FindPath(game.computerPlayer.Position, collectible.TileLocation).Count;
                if (WillExpireSoon(collectible, computerToBDistance)) continue;

                if (computerToBDistance < minComputerToB)
                {
                    minComputerToB = computerToBDistance;
                }
            }

            int minPlayerToA = int.MaxValue;
            foreach (CollectibleItem collectible in game.spawnedCollectibles)
            {
                if (collectible.Type != CollectibleItemType.AddPoint) continue;

                int playerToADistance = pathfinding.FindPath(game.computerPlayer.Position, collectible.TileLocation).Count;
                if (WillExpireSoon(collectible, playerToADistance)) continue;

                if (playerToADistance < minPlayerToA)
                {
                    minPlayerToA = playerToADistance;
                }
            }

            if (minPlayerToA < minComputerToB) Succeed();
            else Fail();
        }
    }
}
