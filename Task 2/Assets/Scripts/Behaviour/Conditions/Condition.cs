﻿namespace AI.Behaviour.Conditions
{
    /// <summary> Behaviour tree nodes which either succeed or fail based on a condition. </summary>
    abstract class Condition : Task { }
}
