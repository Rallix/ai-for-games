﻿using System.Collections.Generic;
using System.Linq;

namespace AI.Behaviour.Processors
{
    /// <summary> A <see cref="Task"/> which evaluates to true when at least one of its children tasks evaluates to true. </summary>
    class Selector : Processor
    {
        Task currentTask = default;
        readonly List<Task> tasks = new List<Task>();

        Queue<Task> taskQueue = new Queue<Task>();

        /// <summary> Creates a new task selector. The order of tasks definies their evaluation order. </summary>
        public Selector(params Task[] selectorTasks)
        {
            tasks.AddRange(selectorTasks);
        }

        /// <summary> Starts running all the selector's tasks until one of them returns true. </summary>
        public override void Start()
        {
            base.Start(); // start the selector

            // Refresh the queue
            taskQueue = new Queue<Task>(tasks);
            currentTask = taskQueue.Dequeue();
            currentTask.Start();
        }

        /// <summary> Keeps making the tasks <see cref="Task.Update"/> until they succeed or there's none left. </summary>
        public override void Update(GameState game)
        {
            currentTask.Update(game);
            if (currentTask.IsRunning) return; // continue if it's still ongoing after acting

            if (currentTask.Succeeded) // done -> finish it
            {
                Succeed(); 
                return;
            }

            if (!taskQueue.Any()) // no more tasks
            {
                State = currentTask.State;
            }
            else // use the next task
            {
                currentTask = taskQueue.Dequeue();
                currentTask.Start();
            }
        }

        /// <summary> Resets all selector's children tasks to their initial state. </summary>
        public override void Reset()
        {
            base.Reset();
            tasks.ForEach(t => t.Reset());
        }
    }
}
