﻿using System.Collections.Generic;
using System.Linq;

namespace AI.Behaviour.Processors
{
    /// <summary> A <see cref="Task"/> which evaluates to true when all of its children tasks evaluates to true. </summary>
    class Sequence : Processor
    {
        Task currentTask = default;
        readonly List<Task> tasks = new List<Task>();

        Queue<Task> taskQueue = new Queue<Task>();

        /// <summary> Creates a new task sequence. The order of tasks definies their evaluation order. </summary>
        public Sequence(params Task[] sequenceTasks)
        {
            tasks.AddRange(sequenceTasks);
        }

        /// <summary> Starts the task. </summary>
        public override void Start()
        {
            base.Start(); // start the sequence

            // Refresh the queue
            taskQueue = new Queue<Task>(tasks);
            currentTask = taskQueue.Dequeue();
            currentTask.Start();
        }

        public override void Update(GameState game)
        {
            currentTask.Update(game);
            if (currentTask.IsRunning) return; // continue if it's still ongoing after acting


            if (currentTask.Failed) // one failed -> all failed
            {
                Fail(); 
                return;
            }

            if (!taskQueue.Any()) // no more tasks
            {
                State = currentTask.State;
            }
            else
            {
                currentTask = taskQueue.Dequeue();
                currentTask.Start();
            }
        }

        /// <summary> Resets all sequence's children tasks to their initial state. </summary>
        public override void Reset()
        {
            base.Reset();
            tasks.ForEach(t => t.Reset());
        }
    }
}
