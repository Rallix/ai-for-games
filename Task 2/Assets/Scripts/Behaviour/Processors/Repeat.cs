﻿using UnityEngine;

namespace AI.Behaviour.Processors
{
    /// <summary> Repeats a task a given number of times. </summary>
    class Repeat : Processor
    {
        readonly Task task;
        readonly int repetitions;
        int i;

        /// <summary> Creates a new repeating task. </summary>
        public Repeat(Task repeatedTask, int times)
        {
            Debug.Assert(times > 1, "It's pointless to repeat a task less than two times.");
            task = repeatedTask;
            repetitions = Mathf.Max(times, 1);
            i = repetitions;
        }

        /// <summary> Creates a new task which repeats itself until it fails. </summary>
        /// <remarks> The game has a short time limit, so using <see cref="int.MaxValue"/> is enough. </remarks>
        public Repeat(Task repeatedTask) : this(repeatedTask, int.MaxValue) { }

        /// <summary> Starts the task. </summary>
        public override void Start()
        {
            base.Start(); // start the repeater
            task.Start(); // start the repeated task
        }

        /// <summary> The action performed by a task while it is running. </summary>
        public override void Update(GameState game)
        {
            if (task.Failed) Fail();
            else if (task.Succeeded)
            {
                if (--i < 1) // all repetitions succeded -> done
                {
                    Succeed();
                    return;
                }
                else // next repetition
                {
                    task.Reset();
                    task.Start();
                }
            }

            if (task.IsRunning) task.Update(game);
        }

        /// <summary> Resets the task to its initial state. </summary>
        public override void Reset()
        {
            base.Reset();
            i = repetitions;
        }
    }
}
