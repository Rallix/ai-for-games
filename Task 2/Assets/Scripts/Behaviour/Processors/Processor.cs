﻿using UnityEngine;

namespace AI.Behaviour.Processors
{
    /// <summary> Behaviour tree nodes which group several nodes together. </summary>
    abstract class Processor : Task
    {
        /// <summary> Starts the task. </summary>
        public override void Start()
        {
            base.Start();
            if (EnableLogs) Debug.Log($"Starting task: <b><color=yellow>{GetType().Name}</color></b>");
        }

        protected override void Succeed()
        {
            base.Succeed();
            if (EnableLogs) Debug.Log($"Task <b>{GetType().Name}</b> ended with <color=green>SUCCESS</color>.");
        }

        protected override void Fail()
        {
            base.Fail();
            if (EnableLogs) Debug.Log($"Task <b>{GetType().Name}</b> ended in <color=red>FAILURE</color>.");
        }
    }
}
