﻿namespace AI.Behaviour
{
    // TODO: To ScriptableObject, so the tree doesn't have to be designed in code
    // [CreateAssetMenu("AI/Behaviour Tree")]
    abstract class Task //: ScriptableObject 
    {
        /// <summary> The internal states of a task. </summary>
        internal enum TaskState
        {
            /// <summary> The task has been created, but not started yet. </summary>
            Initialized,
            /// <summary> The task is still being processed. </summary>
            Running,
            /// <summary> The task finished successfully. </summary>
            Success,
            /// <summary> The task didn't finish. </summary>
            Failure
        }

        /// <summary> Should the task process info be displayed in the console? </summary>
        public static bool EnableLogs { get; set; } = false;

        /// <summary> Creates a new task. Doesn't start it yet.</summary>
        protected Task()
        {
            State = TaskState.Initialized;
        }

        protected internal TaskState State { get; set; }

        public bool IsRunning => State == TaskState.Running;
        public bool Succeeded => State == TaskState.Success;
        public bool Failed => State == TaskState.Failure;
        
        /// <summary> Starts the task. </summary>
        public virtual void Start()
        {
            State = TaskState.Running;
        }

        /// <summary> The action performed by a task while it is running. </summary>
        public abstract void Update(GameState game);

        /// <summary> Resets the task to its initial state. </summary>
        public virtual void Reset()
        {
            State = TaskState.Initialized;
        }

        protected virtual void Succeed()
        {
            State = TaskState.Success;
        }

        protected virtual void Fail()
        {
            State = TaskState.Failure;
        }

        public override string ToString()
        {
            return $"<b>{GetType().Name}</b>";
        }
    }
}
