﻿using System.Collections.Generic;

namespace AI.Behaviour
{
    /// <summary> A wrapper for the parameters passed to the AI. </summary>
    struct GameState
    {
        internal Maze maze;
        internal AbstractPlayer humanPlayer;
        internal ComputerPlayer computerPlayer;
        internal List<CollectibleItem> spawnedCollectibles;
        internal float remainingGameTime;
        internal GameState(Maze maze, AbstractPlayer humanPlayer, ComputerPlayer computerPlayer, List<CollectibleItem> spawnedCollectibles, float remainingGameTime)
        {
            this.maze = maze;
            this.humanPlayer = humanPlayer;
            this.computerPlayer = computerPlayer;
            this.spawnedCollectibles = spawnedCollectibles;
            this.remainingGameTime = remainingGameTime;
        }
    }
}
