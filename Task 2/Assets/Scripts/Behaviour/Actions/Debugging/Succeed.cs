﻿namespace AI.Behaviour.Actions.Debugging
{
    /// <summary> An action which does nothing and instantly succeeds. </summary>
    class Succeed : Action
    {
        /// <summary> The action performed by a task while it is running. </summary>
        public override void Update(GameState game)
        {
            Succeed();
        }
    }
}
