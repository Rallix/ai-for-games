﻿using UnityEngine;

namespace AI.Behaviour.Actions.Debugging
{
    /// <summary> The sole purpose of this debugging <see cref="Task"/> is to yell in the console and return success. </summary>
    class Scream : Action
    {
        readonly ScreamVariant variant;

        public enum ScreamVariant
        {
            Eek,
            Ook,
            Aah
        }

        public Scream(ScreamVariant variant)
        {
            this.variant = variant;
        }

        public Scream() : this(ScreamVariant.Eek) { }

        /// <summary> The action performed by a task while it is running. </summary>
        public override void Update(GameState game)
        {
            string message;
            switch (variant)
            {
                default:
                case ScreamVariant.Eek:
                    message = "<color=orange>Eeeeeek!</color>";
                    break;
                case ScreamVariant.Aah:
                    message = "<color=red>Aaaaaah!</color>";
                    break;
                case ScreamVariant.Ook:
                    message = "<color=cyan>Ooooook!</color>";
                    break;
            }

            Debug.Log($"<b>{message}</b>");
            Succeed();
        }
    }
}
