﻿namespace AI.Behaviour.Actions
{
    /// <summary> Behaviour tree nodes which perform an action. </summary>
    abstract class Action : Task { }
}
