﻿using System.Collections.Generic;
using System.Linq;
using AI.Navigation;
using UnityEngine;

namespace AI.Behaviour.Actions
{
    class NavigateToClosest : Action
    {
        readonly Pathfinding pathfinding;
        readonly CollectibleItemType wanted;
        readonly int maxDistance;
        Queue<Vector2Int> path = new Queue<Vector2Int>();

        const float TimeToMoveOneTile = 0.20f; // on average (not recalculated for simplicity - also the speed doesn't change)

        public NavigateToClosest(Maze maze, CollectibleItemType wantedCollectible, int maxDistance = int.MaxValue)
        {
            pathfinding = new AStar(maze);
            wanted = wantedCollectible;
            this.maxDistance = maxDistance;
        }

        /// <summary> The action performed by a task while it is running. </summary>
        public override void Update(GameState game)
        {
            path = FindPathToClosest(game);

            //! Make step (if possible)
            if (!path.Any())
            {
                Fail(); // we didn't find any valid nearby collectible
            }
            else if (!game.computerPlayer.IsMoving) // don't enqueue another waypoint if the player still moves
            {
                // Make a step
                Vector2Int waypoint = path.Dequeue();
                game.computerPlayer.MakeStep(waypoint);

                if (!path.Any()) Succeed(); // final step towards the collectible
            }
        }

        /// <summary> Finds the path to closest collectible we can get to in time. </summary>
        Queue<Vector2Int> FindPathToClosest(GameState game)
        {
            //! This alone ensures we don't pursue collectibles we can't get to in time. (Task 2.3)
            bool WillExpireSoon(CollectibleItem collectible, int distance)
            {
                return collectible.MarkedForDestroy || collectible.RemainingLifetime < (distance + 1) * TimeToMoveOneTile;
            }

            int min = int.MaxValue;
            Vector2Int currentTile = game.computerPlayer.Position;
            var proposedPath = new Queue<Vector2Int>();
            foreach (CollectibleItem collectible in game.spawnedCollectibles)
            {

                // these aren't the collectibles you're looking for
                if (collectible.Type != wanted && wanted == CollectibleItemType.AddPoint)
                {
                    continue; //! Include closest As in searches for Bs (because As are prioritized)
                }

                Queue<Vector2Int> pathToWanted = pathfinding.FindPath(currentTile, collectible.TileLocation);
                if (WillExpireSoon(collectible, pathToWanted.Count)) continue;


                if (pathToWanted.Count < min && pathToWanted.Count <= maxDistance)
                {
                    min = pathToWanted.Count;
                    proposedPath = pathToWanted;
                }
                else if (pathToWanted.Count == min)
                {
                    // If they're the same, we might want to prefer those 
                    // further from the player in case he gets there first.

                    // On the other hand, it's always good if WE can get there
                    // first (shorter distance than the player) and stop him from scoring.
                }
            }
            return proposedPath;
        }


        /// <summary> Resets the task to its initial state. </summary>
        public override void Reset()
        {
            base.Reset();
            path.Clear();
        }
    }
}
