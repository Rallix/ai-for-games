﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    [Header("Gameplay object references")]
    [SerializeField]
    private Maze maze = default;

    [SerializeField]
    private CollectiblesManager collectiblesManager = default;

    [SerializeField]
    private GameObject humanPlayerPrefab = default;

    [SerializeField]
    private GameObject computerPlayerPrefab = default;

    [Header("Other objects references")]
    [SerializeField]
    private GameObject gameplayCanvas = default;

    [SerializeField]
    private GameObject gameOverCanvas = default;

    [SerializeField]
    private TMPro.TMP_Text timeRemainingText = default;

    [SerializeField]
    private TMPro.TMP_Text humanPlayerPoints = default;

    [SerializeField]
    private TMPro.TMP_Text computerPlayerPoints = default;

    [SerializeField]
    private UnityEngine.UI.Image humanPlayerImage = default;

    [SerializeField]
    private UnityEngine.UI.Image computerPlayerImage = default;

    [SerializeField]
    private TMPro.TMP_Text winnerText = default;

    [SerializeField]
    private TMPro.TMP_Text gameOverPoints = default;

    [SerializeField]
    private UnityEngine.UI.Image winnerImage = default;

    [Header("Gameplay settings")]
    [SerializeField]
    private float movementSpeed = 5.0f;

    [SerializeField]
    private float gameLengthSeconds = 60;

    [SerializeField]
    private int collectiblesMaxCount = 4;

    [SerializeField]
    private float botSpeedModifier = 0.75f;

    [SerializeField]
    private float collectiblesLifetimeSec = 10.0f;

    [SerializeField]
    private float collectibleAddPointSpawnProbability = 0.75f;

    [SerializeField]
    private float collectiblesIndividualRespawnDelay = 2.0f;

    [SerializeField]
    private bool audioEnabled = true;

    public Maze Maze => maze;

    public AbstractPlayer HumanPlayer { get; private set; }

    public AbstractPlayer ComputerPlayer { get; private set; }

    public List<CollectibleItem> SpawnedCollectibles => collectiblesManager.SpawnedCollectibles;

    private Camera _mainCamera;
    public Camera MainCamera
    {
        get
        {
            if(_mainCamera == null)
            {
                _mainCamera = Camera.main;
            }

            return _mainCamera;
        }
    }

    public float TimeRemaining { get; private set; }

    private float lastTimeTextUpdate = float.MaxValue;

    private float timeTextUpdateSpeed = 0.5f;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Debug.LogError("Multiple instanced of game manager occured.");
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        Time.timeScale = 1.0f;
        TimeRemaining = gameLengthSeconds;
        
        gameplayCanvas.SetActive(true);
        gameOverCanvas.SetActive(false);

        StartNewGame();
    }

    private void Update()
    {
        TimeRemaining -= Time.deltaTime;

        TimeTextUpdater();
        CheckGameOver();
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    public void StartNewGame()
    {
        maze.BuildMaze();

        ComputerPlayer = SpawnPlayer(computerPlayerPrefab, maze.ComputerPlayerSpawnTilePos);
        HumanPlayer = SpawnPlayer(humanPlayerPrefab, maze.HumanPlayerSpawnTilePos);

        ComputerPlayer.PointsUpdated += OnPlayerPointsUpdated;
        HumanPlayer.PointsUpdated += OnPlayerPointsUpdated;

        computerPlayerImage.sprite = ComputerPlayer.Sprite;
        humanPlayerImage.sprite = HumanPlayer.Sprite;

        collectiblesManager.InitializeData(maze, collectiblesMaxCount, collectiblesLifetimeSec, 
            collectibleAddPointSpawnProbability, collectiblesIndividualRespawnDelay, HumanPlayer, ComputerPlayer);

        collectiblesManager.RespawnAllCollectibles(1.0f);

        AudioListener.volume = audioEnabled ? 1.0f : 0.0f;

        ComputerPlayer.OnGameStarted();
        HumanPlayer.OnGameStarted();
    }

    public void RestartGame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(
            UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
    }

    private AbstractPlayer SpawnPlayer(GameObject playerPrefab, Vector2Int tilePos)
    {
        var playerGo = Instantiate(playerPrefab, Vector3.zero, Quaternion.identity);

        var playerComp = playerGo.GetComponentInChildren<AbstractPlayer>();

        if (playerComp == null)
        {
            Debug.LogError("Invalid player prefab: " + playerPrefab.name);
        }
        else
        {
            playerComp.InitializeData(maze, movementSpeed * (playerPrefab == humanPlayerPrefab ? 1.0f : botSpeedModifier), tilePos);
        }

        return playerComp;
    }

    private void OnPlayerPointsUpdated(AbstractPlayer playerToUpdate)
    {
        if(playerToUpdate == HumanPlayer)
        {
            humanPlayerPoints.text = HumanPlayer.Points.ToString();
        }
        else if(playerToUpdate == ComputerPlayer)
        {
            computerPlayerPoints.text = ComputerPlayer.Points.ToString();
        }
        else
        {
            Debug.LogError("What magic you used, oh hero, to see this log? I am afraid the path you seek is not right.");
        }
    }

    private void TimeTextUpdater()
    {
        if(lastTimeTextUpdate - TimeRemaining >= timeTextUpdateSpeed)
        {
            timeRemainingText.text = Mathf.Round(TimeRemaining).ToString() + " s";
            lastTimeTextUpdate = TimeRemaining;
        }
    }

    private void CheckGameOver()
    {
        if(TimeRemaining <= 0.0f)
        {
            Time.timeScale = 0;

            string winnerString = "Human wins!";
            string gameOverPointsString = HumanPlayer.Points + " : " + ComputerPlayer.Points;
            Sprite winnerSprite = HumanPlayer.Sprite;

            if(ComputerPlayer.Points > HumanPlayer.Points)
            {
                winnerString = "Computer wins. Machines are the future.";
                gameOverPointsString = ComputerPlayer.Points + " : " + HumanPlayer.Points;
                winnerSprite = ComputerPlayer.Sprite;
            }
            else if(ComputerPlayer.Points == HumanPlayer.Points)
            {
                winnerString = "DRAW! No winners nor losers this time.";
                winnerImage.gameObject.SetActive(false);
            }


            winnerText.text = winnerString;
            gameOverPoints.text = gameOverPointsString;
            winnerImage.sprite = winnerSprite;

            gameplayCanvas.SetActive(false);
            gameOverCanvas.SetActive(true);
        }
    }
}
