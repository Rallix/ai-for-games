﻿using System.Collections.Generic;
using System.Linq;
using AI.Utils;
using UnityEngine;

public class FlockingGameObject : AbstractSteeringGameObject
{
    [SerializeField] protected float neighbourRadius = 8.0f;

    [SerializeField] protected GameObject debugNeighbourRadiusSphere;

    //! Separation >> Cohesion > Alignment

    [Header("Flocking – Separation")]
    [SerializeField, Range(0f, 1f), Tooltip("The importance of separation for the final direction.")]
    float separationWeight = 0.25f;
    [SerializeField] float decayCoefficient = 0.65f;
    [Header("Flocking – Cohesion")]
    [SerializeField, Range(0f, 1f), Tooltip("The importance of cohesion for the final direction.")]
    float cohesionWeight = 0.15f;
    [SerializeField, Range(0, 180), Tooltip("The view angle of the object to use in order to form groups.")]
    float viewAngle = 45;
    [Header("Flocking – Alignment")]
    [SerializeField, Range(0f, 1f), Tooltip("The importance of alignment for the final direction.")]
    float alignmentWeight = 0.10f;

    [Space, SerializeField, Tooltip("Propagate the changes in weighths to all other objects in the flock.")]
    bool sharedWeights = false;

    public FlockSpawner Spawner { get; set; }

    Vector3 gizmoBoxSize = Vector3.zero;

    /// <summary> The center of mass of the object. </summary>
    /// <remarks> Ideally <see cref="Rigidbody.centerOfMass"/>, otherwise position. </remarks>
    public Vector3 CenterOfMass => transform.position;

    void OnEnable()
    {
        gizmoBoxSize = GetComponent<MeshRenderer>().bounds.extents * 2.5f;
    }

    protected override void Start()
    {
        base.Start();

        debugNeighbourRadiusSphere.transform.localScale = Vector3.one * neighbourRadius;

        Velocity = new Vector3(Random.insideUnitSphere.x, 0.0f, Random.insideUnitSphere.y).normalized * maxSpeed;
    }

    protected override void Update()
    {
        base.Update();

        //!     Task 3 Flocking
        //      Use methods below to compute individual steering behaviors.
        //      Information about other agents is stored in "Spawner.Agents".
        //      The variable "neighbourRadius" holds the radius which should be used for neighbour detection.
        //      Set the final velocity to "Velocity" property. The maximum speed of the agent is determined by "maxSpeed".
        //      Feel free to extend the codebase. However, make sure it is easy to find your solution.
        Velocity = GetBlendedDirection(Velocity) * maxSpeed;
        LookDirection = Velocity;
    }

    void OnDrawGizmosSelected()
    {
        if (!Application.isPlaying) return;

        Gizmos.color = Color.green;
        //! Current flocking object
        Gizmos.DrawWireCube(CenterOfMass, gizmoBoxSize);
        AIUtils.GizmosDrawCircle(CenterOfMass, neighbourRadius);

        List<FlockingGameObject> neighbours = GetCloseNeighbours(neighbourRadius).ToList();
        //! Cohesion - global center of mass
        if (!Mathf.Approximately(cohesionWeight, 0))
        {
            Vector3 center = GetAverageCenter(neighbours, viewAngle);

            Gizmos.DrawWireSphere(center, 0.35f);
        }
        //! Alignment - average velocity
        if (!Mathf.Approximately(alignmentWeight, 0))
        {
            Vector3 velocity = GetAverageVelocity(neighbours);
            Debug.DrawRay(CenterOfMass, velocity, Color.green);
        }
    }

    void OnValidate()
    {
        if (sharedWeights)
        {
            foreach (FlockingGameObject agent in Spawner.Agents)
            {
                agent.ApplyWeighths(separationWeight, cohesionWeight, alignmentWeight);
            }
        }
    }

    /// <summary> A simple setter used to change weights of all in the flock. </summary>
    protected void ApplyWeighths(float separation, float cohesion, float alignment)
    {
        separationWeight = separation;
        cohesionWeight = cohesion;
        alignmentWeight = alignment;
    }

    /// <summary> Weights all movement components into a final direction. </summary>
    /// <param name="velocity"> The current velocity. </param>
    Vector3 GetBlendedDirection(Vector3 velocity)
    {
        List<FlockingGameObject> neighbours = GetCloseNeighbours(neighbourRadius).ToList();
        Vector3 blended = velocity.normalized
                        + ComputeSeparation(neighbours) * separationWeight
                        + ComputeCohesion(neighbours) * cohesionWeight
                        + ComputeAlignment(neighbours) * alignmentWeight;
        return blended.normalized;
    }

    /// <summary> Avoid each other in the flock. </summary>
    protected Vector3 ComputeSeparation(IEnumerable<FlockingGameObject> neighbours)
    {
        //! Task 3 Flocking – separation
        Vector3 separation = Velocity;
        foreach (FlockingGameObject neighbour in neighbours)
        {
            Vector3 towards = transform.Towards(neighbour.transform);
            float strength = Mathf.Min(decayCoefficient / towards.sqrMagnitude, maxSpeed); // sqrMagnitude = distance^2
            separation += strength * -towards.normalized;
        }
        return separation.normalized;
    }

    /// <summary> Move the flock slightly towards the collective center of mass. </summary>
    protected Vector3 ComputeCohesion(IEnumerable<FlockingGameObject> neighbours)
    {
        //! Task 3 Flocking – cohesion
        Vector3 averageCenterOfMass = GetAverageCenter(neighbours, viewAngle);
        Vector3 towardsCenter = transform.position.Towards(averageCenterOfMass);

        return towardsCenter.normalized;
    }

    /// <summary> Average the velocity of all nearby objects in a flock. </summary>
    protected Vector3 ComputeAlignment(IEnumerable<FlockingGameObject> neighbours)
    {
        //! Task 3 Flocking – alignment
        Vector3 averageVelocity = GetAverageVelocity(neighbours);
        return averageVelocity.normalized;
    }

    /// <summary> Gets all agents closer than <see cref="neighbourRadius"/> to the current object. </summary>
    IEnumerable<FlockingGameObject> GetCloseNeighbours(float radius)
    {
        var neighbours = new List<FlockingGameObject>();
        if (Spawner == null || Spawner.Agents == null || Spawner.Agents.Count == 0) return neighbours;

        foreach (FlockingGameObject agent in Spawner.Agents)
        {
            if (agent == this) continue;
            if (Vector3.Distance(transform.position, agent.transform.position) < radius)
            {
                neighbours.Add(agent);
            }
        }
        return neighbours;
    }

    /// <summary> Gets all agents closer than <see cref="neighbourRadius"/>
    /// and in field of view of the current object and calculates their average center of mass. </summary>
    Vector3 GetAverageCenter(IEnumerable<FlockingGameObject> neighbours, float fieldOfView = 180)
    {
        Vector3 center = Vector3.zero;
        int count = 0;
        foreach (FlockingGameObject neighbour in neighbours)
        {
            Vector3 towards = transform.Towards(neighbour.transform);
            if (Vector3.Angle(towards, LookDirection) < fieldOfView) // TODO: Half of FOV.
            {
                center += neighbour.CenterOfMass;
                count++;
            }
        }
        return count > 0
                ? center / count
                : Vector3.zero;
    }

    /// <summary> Gets all agents closer than <see cref="neighbourRadius"/>
    /// and calculates their average velocity. </summary>
    Vector3 GetAverageVelocity(IEnumerable<FlockingGameObject> neighbours)
    {
        Vector3 totalVelocity = Vector3.zero;
        int count = 0;
        foreach (FlockingGameObject neighbour in neighbours)
        {
            totalVelocity += neighbour.Velocity;
            count++;
        }
        return count > 0
                ? totalVelocity / count
                : Vector3.zero;
    }

    public override void SetDebugObjectsState(bool newState)
    {
        base.SetDebugObjectsState(newState);
        debugNeighbourRadiusSphere.SetActive(newState);
    }
}
