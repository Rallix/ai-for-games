﻿using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public abstract class AbstractSteeringGameObject : MonoBehaviour
{
    [SerializeField]
    protected Color initColor = Color.white;

    [SerializeField]
    protected float maxSpeed = 3.0f;

    [SerializeField]
    protected TrailRenderer debugTrailRenderer;

    [SerializeField]
    protected LineRenderer debugVelocityDirectionLine;

    protected MeshRenderer meshRenderer;

    private Color _color = Color.clear;
    public virtual Color Color
    {
        get => _color;
        set
        {
            _color = value;
            UpdateMaterialsColors(value);
        }
    }

    public virtual Vector3 Velocity { get; protected set; }

    public virtual Vector3 LookDirection
    {
        get => transform.forward;
        set => transform.forward = value;
    }

    protected virtual void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();

        if (Color == Color.clear)
        {
            Color = initColor;
        }
    }

    protected virtual void Update() { }

    protected virtual void LateUpdate()
    {
        transform.position += Velocity * Time.deltaTime;

        debugVelocityDirectionLine.positionCount = 2;
        debugVelocityDirectionLine.SetPosition(0, Vector3.zero);
        debugVelocityDirectionLine.SetPosition(1, transform.InverseTransformDirection(Velocity.normalized));
    }
    
    public virtual void SetDebugObjectsState(bool newState)
    {
        if(!newState)
        {
            debugTrailRenderer.Clear();
        }
        debugTrailRenderer.gameObject.SetActive(newState);
        debugVelocityDirectionLine.gameObject.SetActive(newState);
    }

    protected virtual void UpdateMaterialsColors(Color newColor)
    {
        if(meshRenderer == null)
        {
            meshRenderer = GetComponent<MeshRenderer>();
        }

        meshRenderer.material.SetColor("_BaseColor", newColor);
        debugTrailRenderer.startColor = newColor;
        debugTrailRenderer.endColor = newColor;
    }
}
