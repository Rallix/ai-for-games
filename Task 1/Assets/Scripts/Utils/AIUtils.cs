﻿using UnityEngine;

namespace AI.Utils
{
    /// <summary> All sorts of simple methods and calculations for convenience and readability. </summary>
    static class AIUtils
    {
        #region Floats

        /// <summary>Remaps a number from a certain range to a new range.</summary>
        /// <param name="value">Value to remap.</param>
        /// <param name="from">The current range for the value.</param>
        /// <param name="to">The new range for the value.</param>
        /// <returns>A value remapped to fit within the specified range.</returns>
        public static float RemapRange(this float value, (float Lower, float Upper) from, (float Lower, float Upper) to)
        {
            return (value - @from.Lower) / (@from.Upper - @from.Lower) * (to.Upper - to.Lower) + to.Lower;
        }

        /// <summary>Remaps a number from a certain range to a range between 1 and 0.</summary>
        /// <param name="value">Value to remap.</param>
        /// <param name="from1">The minimal value of the old value.</param>
        /// <param name="from2">The maximal value of the old value.</param>
        /// <returns>A value remapped to fit the range [0,1].</returns>
        public static float Remap01(this float value, float from1, float from2)
        {
            return value.RemapRange((from1, from2), (0f, 1f));
        }

        #endregion

        #region Vectors

        /// <summary> Returns a vector pointing towards a given target. </summary>
        /// <param name="sourcePosition"> The origin of the vector. </param>
        /// <param name="targetPosition"> Target point to point towards. </param>
        public static Vector3 Towards(this Vector3 sourcePosition, Vector3 targetPosition)
        {
            return targetPosition - sourcePosition;
        }

        /// <summary> Returns a vector pointing towards a given target. </summary>
        /// <param name="source"> The origin of the vector. </param>
        /// <param name="target"> The target to point towards. </param>
        public static Vector3 Towards(this Transform source, Transform target)
        {
            return source.position.Towards(target.position);
        }

        /// <summary> Returns a vector pointing towards a given target, clamped to a given maximum. </summary>
        /// <param name="sourcePosition"> The origin of the vector. </param>
        /// <param name="targetPosition"> Target point. </param>
        /// <param name="maxLength"> Maximum length of a given vector. </param>
        public static Vector3 ClampedTowards(this Vector3 sourcePosition, Vector3 targetPosition, float maxLength)
        {
            Vector3 towards = sourcePosition.Towards(targetPosition);
            return Vector3.ClampMagnitude(towards, maxLength);
        }

        /// <summary> Returns a vector pointing towards a given target, clamped to a given maximum. </summary>
        /// <param name="source"> The origin of the vector. </param>
        /// <param name="target"> The target to point towards. </param>
        /// <param name="maxLength"> Maximum length of a given vector. </param>
        public static Vector3 ClampedTowards(this Transform source, Transform target, float maxLength)
        {
            Vector3 towards = source.Towards(target);
            return Vector3.ClampMagnitude(towards, maxLength);
        }

        /// <summary> Returns the distance between two objects. </summary>
        /// <param name="source"> The source object. </param>
        /// <param name="target"> The target to point towards. </param>
        /// <param name="flat"> Project the distance vector onto a flat plane. </param>
        public static float DistanceTo(this Transform source, Transform target, bool flat = false)
        {
            Vector3 direction = source.Towards(target.transform);
            if (flat)
            {
                direction = Vector3.ProjectOnPlane(direction, source.up);
            }
            return direction.magnitude;
        }

        #endregion

        #region Gizmos

        /// <summary> Draws a flat circle using gizmos. </summary>
        public static void GizmosDrawCircle(Vector3 center, float radius, float step = 0.1f)
        {
            float theta = 0.0f;
            float x = radius * Mathf.Cos(theta);
            float y = radius * Mathf.Sin(theta);
            Vector3 position = center + new Vector3(x, 0, y);
            Vector3 lastPosition = position;
            for (theta = step; theta < Mathf.PI * 2; theta += step)
            {
                x = radius * Mathf.Cos(theta);
                y = radius * Mathf.Sin(theta);
                Vector3 newPosition = center + new Vector3(x, 0, y);
                Gizmos.DrawLine(position, newPosition);
                position = newPosition;
            }
            Gizmos.DrawLine(position, lastPosition);
        }

        #endregion
    }
}
