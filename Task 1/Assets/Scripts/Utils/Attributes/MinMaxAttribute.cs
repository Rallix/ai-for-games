using UnityEngine;

namespace AI
{

    /// <summary> Attribute used to make a <see cref="Vector2"/> variable in a script be restricted to a specific range. </summary>
    /// <exception cref="System.InvalidOperationException"> Thrown when the attribute is used with a wrong property type. </exception>
    public class MinMaxAttribute : PropertyAttribute
    {

        public float minLimit;
        public float maxLimit;
        public bool IntMode { get; } = false;


        /// <summary> Attribute used to make a <see cref="Vector2"/> variable in a script be restricted to a specific range. </summary>
        /// <param name="min">The minimum allowed value.</param>
        /// <param name="max">The maximum allowed value.</param>
        public MinMaxAttribute(float min, float max)
        {
            if (max < min) max = min;
            minLimit = min;
            maxLimit = max;
            IntMode = false;
        }

        public MinMaxAttribute(int min, int max)
        {
            if (max < min) max = min;
            minLimit = min;
            maxLimit = max;
            IntMode = true;
        }

    }

}
