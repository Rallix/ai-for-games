﻿using System;
using UnityEngine;
using UnityEditor;

namespace AI
{

    [CustomPropertyDrawer(typeof(MinMaxAttribute))]
    public class MinMaxDrawer : PropertyDrawer
    {

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var minMax = (MinMaxAttribute) attribute;

            if (property.propertyType == SerializedPropertyType.Vector2)
            {
                float minValue = property.vector2Value.x;
                float maxValue = property.vector2Value.y;
                string rangePreview = minMax.IntMode 
                        ? $"{Mathf.RoundToInt(minValue)} – {Mathf.RoundToInt(maxValue)}" 
                        : $"{minValue:F2} – {maxValue:F2}";
                EditorGUI.LabelField(position, $"{label.text} ({rangePreview})");
                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    EditorGUI.MinMaxSlider(position, " ", ref minValue, ref maxValue, minMax.minLimit, minMax.maxLimit);
                    if (check.changed)
                    {
                        if (minMax.IntMode)
                        {
                            int minValueInt = Mathf.RoundToInt(minValue);
                            int maxValueInt = Mathf.RoundToInt(maxValue);
                            property.vector2Value = new Vector2(minValueInt, maxValueInt);
                        }
                        else
                        {
                            property.vector2Value = new Vector2(minValue, maxValue);
                        }
                    }
                }
            }
            else
            {
                throw new InvalidOperationException($"The MinMax attribute only supports {nameof(Vector2)}, not a <i>{property.propertyType}</i> of {property.name}.");
            }
        }

        // this method lets unity know how big to draw the property. We need to override this because it could end up meing more than one line big
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            // By default just return the standard line height
            float size = EditorGUIUtility.singleLineHeight;
            // size += EditorGUIUtility.singleLineHeight; // If the label was on a separate line

            return size;
        }

    }

}
