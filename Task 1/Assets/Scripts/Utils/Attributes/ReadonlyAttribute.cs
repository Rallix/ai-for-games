using UnityEngine;

/* Use as: [ReadonlyField] public float number; */

namespace AI
{

    public class ReadonlyFieldAttribute : PropertyAttribute { }

}
