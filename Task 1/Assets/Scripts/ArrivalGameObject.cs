﻿using AI;
using AI.Utils;
using UnityEngine;

public class ArrivalGameObject : AbstractSteeringGameObject
{
    [SerializeField] protected GameObject objectToFollow;

    [Header("Arrival")] [ReadonlyField, SerializeField, Tooltip("The current distance between the objects.")] float distance = default;

    [MinMax(0f, 4f), SerializeField, Tooltip("The distance in which the object first slows down, then goes to a full stop.")]
    Vector2 distanceThresholds = new Vector2(0.35f, 1.5f);

    [SerializeField, Tooltip("The curve used to gradually slow down.")] AnimationCurve deccelerateSmoothing = AnimationCurve.EaseInOut(1, 1, 0, 0);

    protected override void Update()
    {
        base.Update();

        //!     Task 1 Arrival 
        //      Add your code here (the object to follow is determined by the value of "objectToFollow" variable).
        //      Set the final velocity to "Velocity" property. The maximum speed of the agent is determined by "maxSpeed".
        //      If you want to change the rotation of the agent, you can use, for example, "LookDirection" property.
        //      Feel free to extend the codebase. However, make sure it is easy to find your solution.

        if (objectToFollow == null)
        {
            Velocity = Vector3.zero;
        }
        else
        {
            distance = transform.DistanceTo(objectToFollow.transform, true);
            Velocity = CalculateVelocity(distance);
            LookDirection = CalculateRotation(transform.ClampedTowards(objectToFollow.transform, 1f),
                                              maxSpeed * Time.deltaTime);
        }
    }

    /// <summary> Calculates the velocity based on given distance thresholds. </summary>
    /// <param name="distance"> The distance between this and the target object. </param>
    Vector3 CalculateVelocity(float distance)
    {
        Vector3 velocity;
        if (distance < distanceThresholds.x)
        {
            // Too close --> stop
            velocity = Vector3.zero;
        }
        else if (distance < distanceThresholds.y)
        {
            // Pretty close --> slow down
            velocity = transform.ClampedTowards(objectToFollow.transform, maxSpeed * GetBrakingModifier(distance));
        }
        else
        {
            // Far enough --> full speed
            velocity = transform.ClampedTowards(objectToFollow.transform, maxSpeed);
        }
        return velocity;
    }

    /// <summary> Calculates a gradual rotation of this object towards a given direction. </summary>
    /// <param name="targetDirection"> The direction to rotate towards. </param>
    /// <param name="step"> Rotational step. </param>
    /// <returns></returns>
    Vector3 CalculateRotation(Vector3 targetDirection, float step)
    {
        return Vector3.RotateTowards(LookDirection, targetDirection, step, 0f);
    }

    /// <summary> Samples the smoothing curve determining the decceleration speed. </summary>
    /// <param name="distance"> The distance between objects.</param>
    /// <returns> A decceleration modifier at the given time. </returns>
    float GetBrakingModifier(float distance)
    {
        float time = Mathf.Clamp01(distance / distanceThresholds.y);
        float firstCurveTime = deccelerateSmoothing[0].time;
        float lastCurveTime = deccelerateSmoothing[deccelerateSmoothing.length - 1].time;

        float curveTime = time.RemapRange(from: (0, 1), to: (firstCurveTime, lastCurveTime));
        return deccelerateSmoothing.Evaluate(curveTime);
    }
}
