﻿using UnityEngine;

public class Sphere
{
    public Collider Collider { get; set; }
    public Vector3 WorldCenter { get; set; }
    public float Radius { get; set; }
}

[SelectionBase]
public class ObstacleContainer : MonoBehaviour { }
