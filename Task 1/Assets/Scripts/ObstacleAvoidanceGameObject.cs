﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using AI;
using AI.Utils;
using UnityEditor;


public class ObstacleAvoidanceGameObject : AbstractSteeringGameObject
{
    [SerializeField] protected LayerMask obstacleLayer;
    [SerializeField] protected float directionChangeDelay = 10.0f;
    [SerializeField] protected LineRenderer debugDesiredDirLine;

    protected Vector3 desiredDirection = Vector3.forward;

    protected Sphere[] obstacles;
    protected Transform container;

    float lastDirectionUpdateTime = 0.0f;

    [Header("Obstacle Avoidance")]

    // [SerializeField, Tooltip("The curve used to smoothen speed while avoiding an obstacle. ")]
    // AnimationCurve avoidanceCurve = AnimationCurve.EaseInOut(0, 1, 0.5f, 0);

    [SerializeField, Range(1, 6), Tooltip("How many rays should the object cast at each side.")]
    int rayCount = 1;
    [SerializeField, Range(0, 30), Tooltip("The total angle covered by the rays.")]
    int raySpread = 60;
    [SerializeField, MinMax(0f, 4f), Tooltip("When to start slowing down and when to come to a full stop and change direction.")]
    Vector2 distanceThresholds = new Vector2(0.75f, 2.5f);
    [SerializeField, Range(0f, 1f), Tooltip("How quickly should the side rays shrink in size.")]
    float shorteningFactor = 1 - 0.65f;

    /// <summary> Calculates rays based on the current desired direction. </summary>
    IEnumerable<Vector3> Rays => GetRays(desiredDirection);

    protected override void Start()
    {
        base.Start();

        // It is not best practice to use LINQ in Unity but it is not a big deal in this case
        // The reason to rather avoid LINQ in Unity is performance & garbage collection
        Selection.activeGameObject = gameObject;
        container = FindObjectOfType<ObstacleContainer>().transform;
        obstacles = container.GetComponentsInChildren<SphereCollider>()
                             .Where(x => obstacleLayer == (obstacleLayer | (1 << x.gameObject.layer)))
                             .Select(x => new Sphere
                              {
                                      Collider = x,
                                      WorldCenter = x.transform.TransformPoint(x.center),
                                      Radius = x.bounds.extents.x
                              })
                             .ToArray();
    }

    protected override void Update()
    {
        base.Update();
        CheckDirectionUpdate();

        //!     Task 2 Obstacle Avoidance 
        //      Information about sphere obstacles is stored in "obstacles" array.
        //      The variable "desiredDirection" holds information about the direction in which the agent wants to move.
        //      Set the final velocity to "Velocity" property. The maximum speed of the agent is determined by "maxSpeed".
        //      Feel free to extend the codebase. However, make sure it is easy to find your solution.

        RaycastHit? closestHit = GetClosestCollision(Rays);
        if (!closestHit.HasValue)
        {
            //! No collisions expected any time soon
            Velocity = desiredDirection * maxSpeed;
        }
        else
        {
            //! Momentarily update the direction
            Vector3 newTarget = closestHit.Value.point + closestHit.Value.normal * distanceThresholds.x;
            Velocity = transform.position.Towards(newTarget).normalized * maxSpeed;
        }
        LookDirection = desiredDirection; // TODO: Look towards the movement direction
    }

    void OnDrawGizmosSelected()
    {
        //! Debug rays
        foreach (Vector3 ray in Rays)
        {
            bool collision = Physics.Raycast(new Ray(transform.position, ray), ray.magnitude, obstacleLayer);
            Debug.DrawRay(transform.position, ray, !collision ? Color.red : Color.blue);
        }
    }

    protected override void LateUpdate()
    {
        base.LateUpdate();

        debugDesiredDirLine.positionCount = 2;
        debugDesiredDirLine.SetPosition(0, Vector3.zero);
        debugDesiredDirLine.SetPosition(1, transform.InverseTransformDirection(desiredDirection.normalized));
    }

    protected void CheckDirectionUpdate()
    {
        if (Time.time - lastDirectionUpdateTime >= directionChangeDelay)
        {
            Vector2 xzDir = Random.insideUnitCircle.normalized;
            desiredDirection = new Vector3(xzDir.x, 0.0f, xzDir.y);
            lastDirectionUpdateTime = Time.time;
        }
    }

    public override void SetDebugObjectsState(bool newState)
    {
        base.SetDebugObjectsState(newState);
        debugDesiredDirLine.gameObject.SetActive(newState);
    }

    /// <summary> Calculates the rays based on the <see cref="rayCount"/>, <see cref="distanceThresholds"/> and <see cref="raySpread"/> parameters. </summary>
    /// <param name="forward">The forward direction of the object.</param>
    IEnumerable<Vector3> GetRays(Vector3 forward)
    {
        if (rayCount == 1)
        {
            return new[] {forward * distanceThresholds.y};
        }
        int rayTotal = rayCount * 2 - 1; // symmetrical

        float halfAngle = raySpread / 2f;
        Quaternion angleIncrement = Quaternion.AngleAxis(raySpread / (float) (rayTotal - 1), transform.up);

        var rays = new Vector3[rayTotal];
        rays[0] = Quaternion.AngleAxis(-halfAngle, transform.up) * (forward * distanceThresholds.y); // leftmost ray
        for (int i = 1; i < rayTotal; i++)
        {
            rays[i] = angleIncrement * rays[i - 1]; // several rays, spread them across given angle
        }
        for (int i = 0; i < rayTotal; i++)
        {
            int shorten = Mathf.Abs(rayTotal / 2 - i); // make side rays smaller, the middle ray longest
            rays[i] *= Mathf.Pow(1 - shorteningFactor, shorten);
        }
        return rays;
    }

    /// <summary> Using given ray directions, raycasts to find obstacles. </summary>
    /// <param name="rays"> Rays to use when checking for obstacles. </param>
    IEnumerable<RaycastHit> GetCollisions(IEnumerable<Vector3> rays)
    {
        var hits = new List<RaycastHit>();
        foreach (Vector3 rayVector in rays)
        {
            var ray = new Ray(transform.position, rayVector);
            if (Physics.Raycast(ray, out RaycastHit rayHit, rayVector.magnitude, obstacleLayer))
            {
                hits.Add(rayHit);
            }
        }
        return hits;
    }

    /// <summary> Using given ray directions, raycasts to find obstacles. </summary>
    /// <param name="rays"> Rays to use when checking for obstacles. </param>
    RaycastHit? GetClosestCollision(IEnumerable<Vector3> rays)
    {
        float length = Mathf.Infinity;
        RaycastHit? hit = null;
        foreach (Vector3 rayVector in rays)
        {
            var ray = new Ray(transform.position, rayVector);
            if (Physics.Raycast(ray, out RaycastHit rayHit, rayVector.magnitude, obstacleLayer) && rayHit.distance < length)
            {
                hit = rayHit;
            }
        }
        return hit;
    }

    /// <summary> Calculates the velocity based on given distance thresholds. </summary>
    /// <param name="hit"> The collision data. </param>
    Vector3 CalculateVelocity(RaycastHit hit)
    {
        float distance = Vector3.Distance(transform.position, hit.point);
        Vector3 newTarget = hit.point + hit.normal * distanceThresholds.y;
        Vector3 newDirection = transform.position.Towards(newTarget).normalized;

        Vector3 velocity;
        if (distance < distanceThresholds.x)
        {
            // Too close --> stop
            velocity = Vector3.zero;
        }
        else if (distance < distanceThresholds.y)
        {
            // Pretty close --> slow down
            velocity = transform.position.ClampedTowards(hit.point, maxSpeed * GetBrakingModifier(distance));
        }
        else
        {
            // Far enough --> full speed
            velocity = transform.position.ClampedTowards(hit.point, maxSpeed);
        }
        return velocity;
    }

    float GetBrakingModifier(float distance)
    {
        return 0.25f;
    }
}
