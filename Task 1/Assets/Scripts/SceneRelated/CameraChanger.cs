﻿using UnityEngine;

public class CameraChanger : MonoBehaviour
{
    [SerializeField]
    private KeyCode keyToSwapCameras = KeyCode.C;

    [SerializeField]
    private Camera defaultOrtographicCamera = default;

    [SerializeField]
    private Camera perspectiveCamera = default;

    private void Update()
    {
        if(Input.GetKeyUp(keyToSwapCameras))
        {
            defaultOrtographicCamera.gameObject.SetActive(!defaultOrtographicCamera.gameObject.activeSelf);
            perspectiveCamera.gameObject.SetActive(!defaultOrtographicCamera.gameObject.activeSelf);
        }
    }
}
