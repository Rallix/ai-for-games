﻿using UnityEngine;

public class SimpleInfiniteArea : MonoBehaviour
{
    private AbstractSteeringGameObject[] steeringGameObjects;

    private Bounds bounds;

    private void Start()
    {
        bounds = GetComponent<MeshRenderer>().bounds;

        steeringGameObjects = FindObjectsOfType<AbstractSteeringGameObject>();
    }

    // Similar functionality can be achieved with colliders & OnTriggerExit but 
    // I wanted to avoid this approach to ensure that the objects students
    // will be using are as simple as possible so they can modify them in case of need.
    private void Update()
    {
        for(int i = 0; i < steeringGameObjects.Length; ++i)
        {
            Vector3 newPosition = steeringGameObjects[i].transform.position;

            if (newPosition.x > bounds.max.x)
            {
                newPosition.x = bounds.min.x;
            }
            else if(newPosition.x < bounds.min.x)
            {
                newPosition.x = bounds.max.x;
            }

            if(newPosition.z > bounds.max.z)
            {
                newPosition.z = bounds.min.z;
            }
            else if(newPosition.z < bounds.min.z)
            {
                newPosition.z = bounds.max.z;
            }

            steeringGameObjects[i].transform.position = newPosition;
        }
    }

    public void ForceObjectsUpdate()
    {
        steeringGameObjects = FindObjectsOfType<AbstractSteeringGameObject>();
    }
}
